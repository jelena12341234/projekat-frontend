import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { CheckAnswer } from '../models/CheckAnswer';

import { FlashMessagesService } from 'angular2-flash-messages';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  ime:string;
  prezime:string;
  lozinka:string;
  ponlozinka:string;
  korime:string;
  datum:string;
  mesto:string;
  mejl:string;
  telefon:string;


  message:string;

  captchaS:string;


  constructor(private myAuthService : AuthServiceService,
             private _flashMessagesService: FlashMessagesService) { }

  ngOnInit(): void {
  }

  recaptcha: any[];
  resolved(captchaResponse: any[]){
    this.captchaS = "OK";
    this.recaptcha= captchaResponse;
    console.log(this.recaptcha);


  }

  registrujSe(){

    if(this.captchaS != "OK") {
      this._flashMessagesService.show('Recaptcha not checked', { cssClass: 'alert-danger', timeout: 3000 });
      return;
    }

    if(this.datum==undefined || this.ime==undefined || this.korime==undefined || this.lozinka==undefined || this.mejl==undefined ||
      this.mesto==undefined || this.ponlozinka==undefined || this.prezime==undefined || this.telefon==undefined) {
        this._flashMessagesService.show('Fields not filled', { cssClass: 'alert-danger', timeout: 3000 });
        return;
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( re.test(String(this.mejl).toLowerCase()) == false){
      this._flashMessagesService.show('Email address not valid', { cssClass: 'alert-danger', timeout: 3000 });
      return;
    }

    if(this.lozinka != this.ponlozinka) {   
      this._flashMessagesService.show('Passwords are not the same', { cssClass: 'alert-danger', timeout: 3000 });   
      return;
    }

    const user = {
      ime : this.ime,
      prezime: this.prezime,
      username: this.korime,
      password: this.lozinka,
      date: this.datum,
      place: this.mesto,
      phone: this.telefon,
      mail: this.mejl, 
      approved: false,
      typeOfUser: "poljoprivrednik"
    }



    this.myAuthService.checkIfUserExists(user).subscribe((answer:CheckAnswer)=>{
      if(answer.success == true){
        this._flashMessagesService.show('There is already user with this username', { cssClass: 'alert-danger', timeout: 3000 });   
      return;
      } 
    });


    this.myAuthService.register(user).subscribe((answer:CheckAnswer)=>{
      if(answer.success == true){
        location.reload();
      }
      else { 
        this._flashMessagesService.show('Registration', { cssClass: 'alert-danger', timeout: 3000 });     
      }
    });
    
location.reload();
  }

}
