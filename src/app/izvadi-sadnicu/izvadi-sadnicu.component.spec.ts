import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IzvadiSadnicuComponent } from './izvadi-sadnicu.component';

describe('IzvadiSadnicuComponent', () => {
  let component: IzvadiSadnicuComponent;
  let fixture: ComponentFixture<IzvadiSadnicuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IzvadiSadnicuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IzvadiSadnicuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
