import { Component, OnInit, Inject } from '@angular/core';
import { RasadniciServiceService } from '../services/rasadnici-service.service';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { sadnicaAnswer } from '../models/sadnicaAnswer';
import { SadnicaInfoComponent } from '../sadnica-info/sadnica-info.component';
import { Sadnica } from '../models/Sadnica';

@Component({
  selector: 'app-izvadi-sadnicu',
  templateUrl: './izvadi-sadnicu.component.html',
  styleUrls: ['./izvadi-sadnicu.component.css']
})
export class IzvadiSadnicuComponent implements OnInit {


  mojaSadnica:Sadnica;

  ngOnInit(): void {
    console.log("Usli smo u ngOnInit");
    this.mojaSadnica = this.myRasService.vratiSadnicu();
  }

  

  constructor( private myRasService:RasadniciServiceService,
    public dialogRef: MatDialogRef<SadnicaInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: sadnicaAnswer) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  izvadi(){
    console.log("usli smo u izvadi");
    this.myRasService.izvadiSadnicu(this.mojaSadnica).subscribe(err=>{
        console.log(err);


      });
    
    //let timerId =  setInterval(function(){
     // this.myRasService.obrisiSadnicu(this.mojaSadnica).subscribe(err=>{
      //  console.log(err);});
    //},4166);




  }

}
