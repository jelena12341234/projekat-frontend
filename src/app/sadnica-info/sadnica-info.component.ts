import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { sadnicaAnswer } from '../models/sadnicaAnswer';
import { RasadniciServiceService } from '../services/rasadnici-service.service';
import { Sadnica } from '../models/Sadnica';

@Component({
  selector: 'app-sadnica-info',
  templateUrl: './sadnica-info.component.html',
  styleUrls: ['./sadnica-info.component.css'],
})
export class SadnicaInfoComponent implements OnInit, OnDestroy {

  mojaSadnica:Sadnica;
 

  timerId:any;


  ngOnInit(): void {
     console.log("Usli smo u ngOnInit");
    this.mojaSadnica = this.myRasService.vratiSadnicu();


    this.timerId= setInterval(()=>{

      this.myRasService.getSadnicaInfo(this.mojaSadnica).subscribe((data:Sadnica[])=>{
        this.mojaSadnica.progress = data[0].progress;
        console.log("dobila novi data");
         console.log(data[0]);
       }, err=>{
         console.log(err);
         return false;
       });
    },10000);
  }

  

  constructor( private myRasService:RasadniciServiceService,
    public dialogRef: MatDialogRef<SadnicaInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: sadnicaAnswer) {
    

    }
  ngOnDestroy(): void {
   clearInterval(this.timerId);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }




}
