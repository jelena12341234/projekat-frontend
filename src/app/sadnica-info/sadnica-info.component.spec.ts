import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SadnicaInfoComponent } from './sadnica-info.component';

describe('SadnicaInfoComponent', () => {
  let component: SadnicaInfoComponent;
  let fixture: ComponentFixture<SadnicaInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SadnicaInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SadnicaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
