import { Component, OnInit, OnDestroy } from '@angular/core';
import { Rasadnik } from '../models/Rasadnik';
import { RasadniciServiceService } from '../services/rasadnici-service.service';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SadnicaInfoComponent } from '../sadnica-info/sadnica-info.component';
import { Sadnica } from '../models/Sadnica';
import { AuthServiceService } from '../services/auth-service.service';
import { Koordinate } from '../models/Koordinate';
import { Router } from '@angular/router';
import { IzvadiSadnicuComponent } from '../izvadi-sadnicu/izvadi-sadnicu.component';

@Component({
  selector: 'app-rasadnik',
  templateUrl: './rasadnik.component.html',
  styleUrls: ['./rasadnik.component.css']
})
export class RasadnikComponent implements OnInit {

  constructor(private myRasService : RasadniciServiceService,
              public dialog:MatDialog,
              private myAuthService: AuthServiceService,
              private route: Router) { }



  myRas : Rasadnik;
  sadnice: Sadnica[][] = [];  
  flags: Boolean[][]=[];
  myCoords: Koordinate;

  ngOnInit(): void {
    this.myRas = this.myRasService.vratiRasadnik();
    this.myRasService.obrisiKoordinate();
    let user = this.myAuthService.loggedInUser();
    
    console.log("row i col rasadnika su"+this.myRas.visina + ","+this.myRas.sirina);

  
    
    for(let i=0; i<this.myRas.visina; i++){
      let ras:Sadnica[]=[]; 
      for(let j=0; j<this.myRas.sirina; j++){
        ras.push(null);
      }
      this.sadnice.push(ras);
    }

    
    for(let i=0; i<this.myRas.visina; i++){
      let bool:Boolean[]=[]; 
      for(let j=0; j<this.myRas.sirina; j++){
        bool.push(false);
      }
      this.flags.push(bool);
    }


    for(let i=0; i<this.myRas.visina; i++){
      let asd = {
        username:user.username,
        nazivRasadnika: this.myRas.naziv,
        mesto: this.myRas.mesto, 
        row:i      
      }

      this.myRasService.vratiSadnice(asd).subscribe((data:Sadnica[])=>{
      //this.sadnice[asd.row] = data;

      for(let j=0;j<data.length; j++){
        console.log("row i col su: " + asd.row+","+data[j].col);
        this.flags[asd.row][data[j].col] = true;
        this.sadnice[asd.row][data[j].col] = data[j];
      }

    }, err=>{
      console.log(err);
      return false;
    });
    
  }


  }


  changeTemp(str){
    if(str=='plus'){
      console.log("change temp u plus");
      this.myRasService.povecajTemp(this.myRas).subscribe((data:Rasadnik)=>{
        this.myRas.temperatura = this.myRas.temperatura + 1;
        this.myRasService.obrisiRasadnik();
        this.myRasService.ucitajRasadnik(this.myRas);
      }, err=>{
        console.log(err);
        return false;
      });
    } 
    if(str=='minus'){
      this.myRasService.smanjiTemp(this.myRas).subscribe((data:Rasadnik)=>{
        this.myRas.temperatura = this.myRas.temperatura - 1;
        this.myRasService.obrisiRasadnik();
        this.myRasService.ucitajRasadnik(this.myRas);
    }, err=>{
      console.log(err);
      return false;
    });
    }


//    location.reload();
  }

  changeWater(str){
    if(str=='plus'){
      this.myRasService.povecajVodu(this.myRas).subscribe((data:Rasadnik)=>{
        this.myRas.kolVode = this.myRas.kolVode + 1;
        this.myRasService.obrisiRasadnik();
        this.myRasService.ucitajRasadnik(this.myRas);
      }, err=>{
      console.log(err);
      return false;
    });
    } 
    if(str=='minus'){
      this.myRasService.smanjiVodu(this.myRas).subscribe((data:Rasadnik)=>{
        this.myRas.kolVode = this.myRas.kolVode - 1;
        this.myRasService.obrisiRasadnik();
        this.myRasService.ucitajRasadnik(this.myRas);
      }, err=>{
      console.log(err);
      return false;
    });
    }
    
  }


  showInfo(i,j){

    let user = this.myAuthService.loggedInUser();

   this.myRasService.ucitajSadnicu(this.sadnice[i][j]);
   console.log("sadnica ucitana: ");
   console.log(this.sadnice[i][j]);

   if(this.sadnice[i][j].progress<100){
    const dialogRef = this.dialog.open(SadnicaInfoComponent);


    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.myRasService.obirsiSadnicu();
      console.log("sadnica obrisana");

      
    

    
  });
  
   // this.dialog.open(SadnicaInfoComponent);
  } else{
    const dialogRef = this.dialog.open(IzvadiSadnicuComponent);


  

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.myRasService.obirsiSadnicu();
      console.log("sadnica obrisana");

      for(let i=0; i<this.myRas.visina; i++){
        let ras:Sadnica[]=[]; 
        for(let j=0; j<this.myRas.sirina; j++){
          ras.push(null);
        }
        this.sadnice[i]=ras;
      }
  

      for(let i=0; i<this.myRas.visina; i++){
        let asd = {
          username:user.username,
          nazivRasadnika: this.myRas.naziv,
          mesto: this.myRas.mesto, 
          row:i      
        }
  
  
        this.myRasService.vratiSadnice(asd).subscribe((data:Sadnica[])=>{
          //this.sadnice[asd.row] = data;
    
          for(let j=0;j<data.length; j++){
            console.log("row i col su: " + asd.row+","+data[j].col);
            this.flags[asd.row][data[j].col] = true;
            this.sadnice[asd.row][data[j].col] = data[j];
          }
    
        }, err=>{
          console.log(err);
          return false;
        }); 
        
  
      }

      
    let timerId = setInterval(function(){

    },4166);

      setTimeout(() => {
        clearInterval(timerId);
        this.myRasService.obrisiSadnicu(this.sadnice[i][j]).subscribe(err=>{
          this.sadnice[i][j]=null;
   //       location.reload();
          console.log(err)
        });
      
  
      },4167);




      
    });

  }
  }


  zasadi(i,j){
    console.log("usli smo u zasadi sa koord "+i+","+j);
    let kord={
      koord1:i,
      koord2:j
    }
    this.myCoords = kord;
 //   this.myCoords.koord1=i; //red
   // this.myCoords.koord2=j; //kol
   
   console.log("this.myCoords.koord1"+this.myCoords.koord1);
   console.log("this.myCoords.koord2"+this.myCoords.koord2);
   
   this.myRasService.ucitajKoordinate(this.myCoords);
  
   this.route.navigate(['/magacin']);


  }

}
