import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { RecaptchaModule} from 'ng-recaptcha';
import { RegisterPredComponent } from './register-pred/register-pred.component';
import { AdminComponent } from './admin/admin.component';
import { PoljoprivrednikComponent } from './poljoprivrednik/poljoprivrednik.component';
import { PreduzeceComponent } from './preduzece/preduzece.component';
import { RasadnikComponent } from './rasadnik/rasadnik.component';



import { FlashMessagesModule } from 'angular2-flash-messages';
import {MatDialogModule} from '@angular/material/dialog';
import { SadnicaInfoComponent } from './sadnica-info/sadnica-info.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DodajRasadnikComponent } from './dodaj-rasadnik/dodaj-rasadnik.component'; 

import { PogledajproizvodeComponent } from './pogledajproizvode/pogledajproizvode.component';
import { MagacinComponent } from './magacin/magacin.component';
import { OnlineProdavnicaComponent } from './online-prodavnica/online-prodavnica.component';
import { ProizvodiInfoComponent } from './proizvodi-info/proizvodi-info.component';
import { AddProductComponent } from './add-product/add-product.component';

import {MatStepperModule} from '@angular/material/stepper';
import { HomeComponent } from './home/home.component';
import { IzvadiSadnicuComponent } from './izvadi-sadnicu/izvadi-sadnicu.component';
import { DataVisualComponent } from './data-visual/data-visual.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    RegisterPredComponent,
    AdminComponent,
    PoljoprivrednikComponent,
    PreduzeceComponent,
    RasadnikComponent,
    DodajRasadnikComponent,
    PogledajproizvodeComponent,
    MagacinComponent,
    OnlineProdavnicaComponent,
    ProizvodiInfoComponent,
    AddProductComponent,
    HomeComponent,
    IzvadiSadnicuComponent,
    DataVisualComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RecaptchaModule,
    HttpClientModule,
    FlashMessagesModule.forRoot(),
    MatDialogModule,
    BrowserAnimationsModule, 
    MatStepperModule
  ],
  entryComponents: [
    SadnicaInfoComponent,
    IzvadiSadnicuComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
