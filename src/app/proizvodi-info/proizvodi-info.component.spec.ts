import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProizvodiInfoComponent } from './proizvodi-info.component';

describe('ProizvodiInfoComponent', () => {
  let component: ProizvodiInfoComponent;
  let fixture: ComponentFixture<ProizvodiInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProizvodiInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProizvodiInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
