import { Component, OnInit } from '@angular/core';
import { user } from '../models/user';
import { AuthServiceService } from '../services/auth-service.service';
import { Proizvod } from '../models/Proizvod';
import { ShopService } from '../services/shop.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-proizvodi-info',
  templateUrl: './proizvodi-info.component.html',
  styleUrls: ['./proizvodi-info.component.css']
})
export class ProizvodiInfoComponent implements OnInit {
  
  korisnik:user;
  myProducts:Proizvod[]=[];

  constructor(private myAuthService: AuthServiceService,
              private myShopService: ShopService,
              private route:Router) { }

    logout(){
      this.myAuthService.logOut();
    }

  ngOnInit(): void {
  this.korisnik = this.myAuthService.loggedInUser();
    
  this.myShopService.getMyProducts(this.korisnik).subscribe((data:Proizvod[])=>{
    this.myProducts = data;
  }, err=>{
    console.log(err);
  });
}


  view(ras){
    this.myShopService.deleteProduct();
    this.myShopService.setProduct(ras);
    this.route.navigate(['/viewproduct']);    
  }

}
