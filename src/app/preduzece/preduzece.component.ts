import { Component, OnInit } from '@angular/core';
import { Narudzbina } from '../models/Narudzbina';
import { ShopService } from '../services/shop.service';
import { user } from '../models/user';
import { Users } from '../models/Users';
import { AuthServiceService } from '../services/auth-service.service';
import { Koordinate } from '../models/Koordinate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preduzece',
  templateUrl: './preduzece.component.html',
  styleUrls: ['./preduzece.component.css']
})
export class PreduzeceComponent implements OnInit {

  myOrders: Narudzbina[] = [];
  priorityOrders: Narudzbina[] = [];
 


  korisnik: user;
  brKurira: number = 5;

  mesto1: string;
  mesto2: string;

  coord  :Koordinate;


  broj: number = 0;

  regularFlags:Boolean[]=[];
  priorityFlags:Boolean[]=[];

  coords1X:number;
  coords1Y:number;
  coords2X:number;
  coords2Y:number;




  constructor(private myShopService: ShopService,
    private myAuthService: AuthServiceService,
    private route:Router) { }

    logout(){
      this.myAuthService.logOut();
    }


  ngOnInit(): void {
    this.korisnik = this.myAuthService.loggedInUser();
    
    let info = {
      proizvodjac: this.korisnik.username
    }
    this.myShopService.getOrders(this.korisnik).subscribe((data: Narudzbina[]) => {
      this.myOrders = data; 
     }, err => { console.log(err); return false; })
  }

  view(){
    this.route.navigate(['/mojiproizvodi']);
  }

  addProduct(){
    this.route.navigate(['/addproduct']);
  }

  sort() {

    this.myShopService.sortOrders(this.korisnik).subscribe((data: Narudzbina[]) => {
      this.myOrders = data;
      for(let i=0; i < this.myOrders.length; i++){
        this.regularFlags.push(false);
      }
    }, err => { console.log(err); return false; });
  }

  vreme(coord1X,coord1Y,coord2X,coord2Y) {
    let testt = /"travelDuration":\d+.\d+/;
    let podtest = /\d+.\d+/;
    this.myShopService.timeNeeded(coord1X,coord1Y,coord2X,coord2Y).subscribe(data => {
      console.log(JSON.stringify(data));
      if (testt.test(JSON.stringify(data)) == true) {
        let pom = JSON.stringify(data).match(testt)[0];
        let pom2 = pom.match(podtest)[0];
        this.broj = Number(pom2);
      } else { console.log("nisam usao"); }
    });
  }

  mestoNarucioca(str) {
    let testt = /"coordinates":\[\d+.\d+,\d+.\d+\]/;
    let podtest = /\d+.\d+,\d+.\d+/;
    let napokon = /\d+.\d+/g;
    let koord1 = 0;
    let koord2 = 0;
    let zamenio = 0;
  
    this.myShopService.Coords(str).subscribe(data => {
      let pom = JSON.stringify(data).match(testt)[0];
      let pom2 = pom.match(podtest)[0];
      let pom3 = pom2.match(napokon)[0]; let pom4 = pom2.match(napokon)[1];
      koord1 = Number(pom3); koord2 = Number(pom4);
      this.coords2X = koord1; this.coords2Y=koord2;
      zamenio = 1;
    });
  }

  mestoPosiljaoca(str) {
  }



  decline(ras,i,order) {
    
    if(order=="regular"){
      this.myOrders.splice(i,1);
      } else if(order=="priority"){
      this.priorityOrders.splice(i,1);  
      }
      
    this.myShopService.decline(ras).subscribe(err => {
      console.log(err);
      return false;
    });
    location.reload();
  }


  accept(ras, i, order) {
    console.log("broj kurira je "+this.brKurira);

    if (this.brKurira > 0) {
      this.brKurira = this.brKurira - 1;

      if(order=="regular"){
        this.regularFlags[i]=true;  
      } else if(order=="priority"){
        this.priorityFlags[i]=true;
        }


        let usr1 = {
          username: this.korisnik.username
        }
        this.myShopService.getUser(usr1).subscribe((data: Users) => {
          this.mesto1 =data.place;
          
        let usr2 = {
          username: ras.username
        }
        this.myShopService.getUser(usr2).subscribe((data: Users) => {
          this.mesto2 = data.place;
          console.log("place one and place two: " + this.mesto1 + " " + this.mesto2);
  
          let testt = /"coordinates":\[\d+.\d+,\d+.\d+\]/;
          let podtest = /\d+.\d+,\d+.\d+/;
          let napokon = /\d+.\d+/g;
          let koord1 = 0;
          let koord2 = 0;
          let zamenio = 0;
        
          this.myShopService.Coords(this.mesto2).subscribe(data => {
            let pom = JSON.stringify(data).match(testt)[0];
            let pom2 = pom.match(podtest)[0];
            let pom3 = pom2.match(napokon)[0]; let pom4 = pom2.match(napokon)[1];
            koord1 = Number(pom3); koord2 = Number(pom4);
            this.coords2X = koord1; this.coords2Y=koord2;
            zamenio = 1;

            
            this.myShopService.Coords(this.mesto1).subscribe(data => {
            let testt = /"coordinates":\[\d+.\d+,\d+.\d+\]/;
            let podtest = /\d+.\d+,\d+.\d+/;
            let napokon = /\d+.\d+/g;
            let koord1 = 0;
            let koord2 = 0;
            let zamenio = 0;
            let pom = JSON.stringify(data).match(testt)[0];
            let pom2 = pom.match(podtest)[0];
            let pom3 = pom2.match(napokon)[0]; let pom4 = pom2.match(napokon)[1];
            koord1 = Number(pom3); koord2 = Number(pom4);
            this.coords1X = koord1; this.coords1Y=koord2;
            zamenio = 1;
            
            console.log("Koordinate posiljaoca su" + this.coords1X + "," + this.coords1Y);
            console.log("Koordinate narucioca su" + this.coords2X + "," + this.coords2Y);

            this.myShopService.timeNeeded( this.coords1X,this.coords1Y,this.coords2X, this.coords2Y).subscribe(data => {
            let testt = /"travelDuration":\d+.\d+/;
            let podtest = /\d+.\d+/;
              console.log(JSON.stringify(data));
              if (testt.test(JSON.stringify(data)) == true) {
                let pom = JSON.stringify(data).match(testt)[0];
                let pom2 = pom.match(podtest)[0];
                this.broj = Number(pom2);
              } else { console.log("nisam usao"); }
             
            console.log("vreme potrebno od "+this.mesto1+ " do "+this.mesto2+ " je "+this.broj);              
              let vreme = this.broj*100;    //radi simulacije minuti su pomnozeni sa 100
           
           
      let timerId = setInterval(function () {
      }, vreme);


      setTimeout(() => {
        clearInterval(timerId);
        this.brKurira=this.brKurira+1;

        if(order=="regular"){
        let index = this.myOrders.indexOf(ras);
        this.myOrders.splice(index,1);
        this.regularFlags.splice(index,1);
        console.log("obirsan iz myOrders "+index+"-ti element");
        } else if(order=="priority"){
        let index = this.priorityOrders.indexOf(ras);
        this.priorityOrders.splice(index,1);
        this.priorityFlags.splice(index,1);  
        console.log("obirsan iz prioritetyOrders "+index+"-ti element");
        }
        
       /* 
      this.myShopService.delete(ras).subscribe(err => {
          console.log(err);
        return false;
        });

        //za potrebe testiranja je komentarisano
        */
      
      //namesti kolicinu
        //namesti da je stigla arrived==true
      }, vreme);
           
           
           
           
            });
          });
          });
        }, err => {
          console.log(err);
        });  



      },err=>{
        console.log(err);
      });
      
      //izracunaj vreme
      let vreme = 15000;


    } else {
        if(order=="regular"){
        let index = this.myOrders.indexOf(ras);
        this.myOrders.splice(index,1);
        this.regularFlags.splice(index,1);
        console.log("obirsan iz myOrders "+index+"-ti element");
        } else if(order=="priority"){
        let index = this.priorityOrders.indexOf(ras);
        this.priorityOrders.splice(index,1);
        this.priorityFlags.splice(index,1);  
        console.log("obirsan iz prioritetyOrders "+index+"-ti element");
        }
        
      this.priorityOrders.push(ras);
      this.priorityFlags.push(false);
    }


  }

}
