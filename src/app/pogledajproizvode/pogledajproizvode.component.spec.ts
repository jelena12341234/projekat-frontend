import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PogledajproizvodeComponent } from './pogledajproizvode.component';

describe('PogledajproizvodeComponent', () => {
  let component: PogledajproizvodeComponent;
  let fixture: ComponentFixture<PogledajproizvodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PogledajproizvodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PogledajproizvodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
