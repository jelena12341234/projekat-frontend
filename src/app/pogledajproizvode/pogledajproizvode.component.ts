import { Component, OnInit } from '@angular/core';
import { ShopService } from '../services/shop.service';
import { Komentar } from '../models/Komentar';
import { Proizvod } from '../models/Proizvod';
import { AuthServiceService } from '../services/auth-service.service';

@Component({
  selector: 'app-pogledajproizvode',
  templateUrl: './pogledajproizvode.component.html',
  styleUrls: ['./pogledajproizvode.component.css']
})
export class PogledajproizvodeComponent implements OnInit {

  myComments:Komentar[]=[];
  myProduct:Proizvod;

  constructor(private myShop:ShopService,
              private myAuthService:AuthServiceService) { }

    logout(){
      this.myAuthService.logOut();
    }

  ngOnInit(): void {

    this.myProduct = this.myShop.getProduct();
    this.myShop.getComments(this.myProduct).subscribe((data:Komentar[])=>{
      this.myComments = data;
    }, err=>{
      console.log(err);
    });

  }

}
