import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { RasadniciServiceService } from '../services/rasadnici-service.service';
import { user } from '../models/user';


import { FlashMessagesService } from 'angular2-flash-messages';



@Component({
  selector: 'app-dodaj-rasadnik',
  templateUrl: './dodaj-rasadnik.component.html',
  styleUrls: ['./dodaj-rasadnik.component.css']
})
export class DodajRasadnikComponent implements OnInit {

  constructor(private myAuthService: AuthServiceService,
              private myRasService: RasadniciServiceService,
              private _flashMsg: FlashMessagesService) { }

  ngOnInit(): void {
    this.korisnik = this.myAuthService.loggedInUser();
    if(this.korisnik == null) {
      this._flashMsg.show("Greška pri očitavanju korisnika", { cssClass: 'alert-danger', timeout: 3000 });
    }
  }

  logout(){
    this.myAuthService.logOut();
  }

  naziv:string;
  mesto:string;
  duzina:string;
  sirina:string;

  korisnik:user;

  dodajRasadnik(){

    let len = parseInt(this.duzina);
    let wid = parseInt(this.sirina);
    

    let numOfSad = len*wid;

      let ras = {
        naziv:this.naziv,
        username: this.korisnik.username,
        mesto: this.mesto,
        brojSadnica:0,
        brojSlMesta:numOfSad,
        kolVode:200,
        temperatura:18
      }

      this.myRasService.dodajRasadnik(ras).subscribe(err=>{
        console.log(err);
        return false;
      });

      location.reload();
} 


}
