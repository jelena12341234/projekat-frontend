import { Component, OnInit } from '@angular/core';
import { Rasadnik } from '../models/Rasadnik';
import { Users } from '../models/Users';
import { LoginAnswer } from '../models/LoginAnswer';
import { Poljoprivrednik } from '../models/Poljoprivrednik';
import { RasadniciServiceService } from '../services/rasadnici-service.service';
import { user } from '../models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-poljoprivrednik',
  templateUrl: './poljoprivrednik.component.html',
  styleUrls: ['./poljoprivrednik.component.css']
})
export class PoljoprivrednikComponent implements OnInit {

  constructor(private myRasService: RasadniciServiceService,
              private route:Router) { }

  rasadnici:Rasadnik[];
  korisnik:user;

  
  ngOnInit(): void {
    this.korisnik = JSON.parse(localStorage.getItem('korisnik'));
   // localStorage.setItem('korisnik',null);
  
    this.myRasService.vratiRasadnike(this.korisnik).subscribe((data: Rasadnik[])=>{
      this.rasadnici=data;
      console.log(data);
    }, err=>{
      console.log(err);
      return false;
    });
  }


  show(ras){
    this.myRasService.obrisiRasadnik();
    this.myRasService.ucitajRasadnik(ras);
    this.route.navigate(['/rasadnik']);
  }


  add(){
    this.route.navigate(['/dodajras']);
  }

}
