import { Component, OnInit, OnDestroy } from '@angular/core';
import { user } from '../models/user';
import { Rasadnik } from '../models/Rasadnik';
import { RasadniciServiceService } from '../services/rasadnici-service.service';
import { MagacinAnswer } from '../models/MagacinAnswer';
import { Koordinate } from '../models/Koordinate';
import { Router } from '@angular/router';

@Component({
  selector: 'app-magacin',
  templateUrl: './magacin.component.html',
  styleUrls: ['./magacin.component.css']
})
export class MagacinComponent implements OnInit {

  constructor(private myRasService : RasadniciServiceService,
              private route:Router) { }


  korisnik:user;
  mojRas:Rasadnik;
  mojiProizvodi:MagacinAnswer[] = [];

  sortBy:string;
  filterByP:string;
  filterByK:string;
  filterByN:string;
  

  filtrirajP:string[] = [];
  filtrirajK:Number[] = [];
  filtrirajN:string[] = [];


  myCoords:Koordinate;




  zasadi(proizvod){
    if(proizvod.kolicina<=0) { alert("Nemate vise na stanju"); return; }
    console.log(this.myCoords);

     let sad={
       username:this.korisnik.username,
       nazivRasadnika:this.mojRas.naziv,
       mesto: this.mojRas.mesto,
       proizvodjac: proizvod.proizvodjac,
       naziv : proizvod.naziv,
       progress:0,
       row: this.myCoords.koord1,
       col: this.myCoords.koord2
     }


     this.myRasService.dodajSadnicu(sad).subscribe(err=>{
        console.log(err);
     });

     let ras={
      naziv:this.mojRas.naziv,
      brojSadnica:this.mojRas.brojSadnica + 1,
      brojSlMesta:this.mojRas.brojSlMesta - 1
     }

     this.myRasService.promeniBrSadnica(ras).subscribe(err=>{
        console.log(err);
     });

      let mag={
        imeSadnice : proizvod.naziv,
        username : this.korisnik.username,
        kolicina : proizvod.kolicina - 1,
        nazivRasadnika : this.mojRas.naziv
      }

     this.myRasService.changeQuanity(mag).subscribe(err=>{
      console.log(err);
     });



    this.myRasService.obrisiKoordinate();
    this.route.navigate(['/rasadnik']);
  }



  ngOnInit(): void {
    this.korisnik = JSON.parse(localStorage.getItem('korisnik'));
    this.mojRas = this.myRasService.vratiRasadnik();
    this.myCoords = this.myRasService.vratiKoordinate();

    let usr = {
     username:this.korisnik.username,
     nazivRasadnika:this.mojRas.naziv
    }

    this.myRasService.getMagacinInfo(usr).subscribe((data:MagacinAnswer[])=>{
      this.mojiProizvodi=data;

      this.mojiProizvodi.forEach(proizvod=>{
        let flag=false;  
       this.filtrirajP.forEach(filproizvod=>{
         if(filproizvod == proizvod.proizvodjac) flag=true;
         });
 
         if(!flag) this.filtrirajP.push(proizvod.proizvodjac);
     });
 

     this.mojiProizvodi.forEach(proizvod=>{
      let flag=false;  
     this.filtrirajK.forEach(filproizvod=>{
       if(filproizvod == proizvod.kolicina) flag=true;
       });

       if(!flag) this.filtrirajK.push(proizvod.kolicina);
   });

   this.mojiProizvodi.forEach(proizvod=>{
    let flag=false;  
   this.filtrirajK.forEach(filproizvod=>{
     if(filproizvod == proizvod.kolicina) flag=true;
     });

     if(!flag) this.filtrirajK.push(proizvod.kolicina);
 });


 this.mojiProizvodi.forEach(proizvod=>{
  let flag=false;  
 this.filtrirajN.forEach(filproizvod=>{
   if(filproizvod == proizvod.naziv) flag=true;
   });

   if(!flag) this.filtrirajN.push(proizvod.naziv);
});


  },err=>{
      console.log(err);
      return false;
    }); 



  }

  filter(){
    if(this.filterByN=="..po nazivu" && this.filterByP=="..po proizvođaču" && this.filterByK=="..po količini") return;

    let usr = {
      username:this.korisnik.username,
      nazivRasadnika:this.mojRas.naziv,
      naziv:this.filterByN,
      kolicina:this.filterByK,
      proizvodjac:this.filterByP
     }

     console.log(usr);


     this.myRasService.filter(usr).subscribe((data:MagacinAnswer[])=>{
        this.mojiProizvodi=data;
     },err=>{
      console.log(err);
      return false;
     });
    }


  sort(){

    let usr = {
      username:this.korisnik.username,
      nazivRasadnika:this.mojRas.naziv
     }

     
    if(this.sortBy == "..po nazivu"){
    this.myRasService.sortByNaziv(usr).subscribe((data:MagacinAnswer[])=>{
      console.log(data);
      this.mojiProizvodi=data;
    },err=>{
      console.log(err);
      return false;
    });

    } else if(this.sortBy == "..po proizvođaču"){
      this.myRasService.sortByProizvodjac(usr).subscribe((data:MagacinAnswer[])=>{
      this.mojiProizvodi=data;
      },err=>{
        console.log(err);
        return false;
      });
    } else if(this.sortBy == "..po količini"){
      this.myRasService.sortByKolicina(usr).subscribe((data:MagacinAnswer[])=>{
      this.mojiProizvodi=data;
      },err=>{
        console.log(err);
        return false;
      });
    }

  }



}
