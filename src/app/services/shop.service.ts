import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor(private _http:HttpClient) { }

  
    dodajProizvod(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/proizvodi/addProizvod',ras,{headers:headers});
  }


  vratiProizvode(){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.get('http://localhost:3000/proizvodi/getProizvodi',{headers:headers});
  }

  getMyProducts(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/proizvodi/getMyProducts',ras,{headers:headers});
  }

  getComments(info){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/komentari/getComms',info,{headers:headers});
  }

  getOrders(name){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/orders/getOrders',name,{headers:headers});  
  }


  sortOrders(name){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/orders/sort',name,{headers:headers});  
  }

  timeNeeded(coord1X,coord1Y,coord2X,coord2Y){
    console.log(coord1X+ "," + coord1Y+";"+coord2X+","+coord2Y);
    return this._http.get("https://dev.virtualearth.net/REST/v1/Routes/DistanceMatrix?origins="+coord1X+","+coord1Y+"&destinations="+coord2X+","+coord2Y+"&travelMode=driving&startTime=2017-06-15T13:00:00-07:00&key=ApR2Bzoer9XWK1SrqC_3nt7ve9DxP2qC0gs-h7Ba8qP4gH1TJTonV3MsrBTouc2l");
  }


  Coords(str){
    return this._http.get("http://dev.virtualearth.net/REST/v1/Locations?locality="+str+"&maxResults=1&key=ApR2Bzoer9XWK1SrqC_3nt7ve9DxP2qC0gs-h7Ba8qP4gH1TJTonV3MsrBTouc2l");
  }


  accept(){

  }

  decline(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/orders/decline',ras,{headers:headers});      
  }

  delete(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/orders/decline',ras,{headers:headers});        
  }


  getUser(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/getUser',ras,{headers:headers});      
  }

  setProduct(ras){
    localStorage.setItem('product',JSON.stringify(ras));
  }

  getProduct(){
    return JSON.parse(localStorage.getItem('product'));
  }

  deleteProduct(){
    localStorage.setItem('product',null);
  }



}