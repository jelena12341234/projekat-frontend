import { TestBed } from '@angular/core/testing';

import { RasadniciServiceService } from './rasadnici-service.service';

describe('RasadniciServiceService', () => {
  let service: RasadniciServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RasadniciServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
