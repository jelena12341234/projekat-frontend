import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class RasadniciServiceService {

  constructor(private _http: HttpClient) { }

  dodajSadnicu(sad){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/sadnice/dodajSadnicu',sad,{headers:headers});
  }
  
  
  vratiSadnice(user){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/sadnice/vratiSadnice',user,{headers:headers});
  }


  izvadiSadnicu(sad){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/sadnice/izvadi',sad,{headers:headers});
  }

  
  obrisiSadnicu(sad){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/sadnice/obrisi',sad,{headers:headers});
  }
  
  

  vratiRasadnike(user){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/vratiRasadnike',user,{headers:headers});
  }


  povecajTemp(ras){
    console.log("usli u servis za +T");
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/povecajTemp',ras,{headers:headers});
 
  }

  smanjiTemp(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/smanjiTemp',ras,{headers:headers}); 
  }

  promeniBrSadnica(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/changeNumSadnicas',ras,{headers:headers});
  }

  povecajVodu(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/povecajVodu',ras,{headers:headers});
  }

  smanjiVodu(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/smanjiVodu',ras,{headers:headers});
  }

  dodajRasadnik(ras){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/rasadnici/dodajRasadnik',ras,{headers:headers});  
  }

  ucitajKoordinate(coord){
    localStorage.setItem('coord',JSON.stringify(coord));
  }
  obrisiKoordinate(){
    localStorage.setItem('coord',null);
  }
  vratiKoordinate(){
    return JSON.parse(localStorage.getItem('coord'));
  }


  ucitajRasadnik(ras){
    localStorage.setItem('rasadnici',JSON.stringify(ras));
  }

  obrisiRasadnik(){
    localStorage.setItem('rasadnici',null);
  }

  vratiRasadnik(){
    return JSON.parse(localStorage.getItem('rasadnici'));
  }

  ucitajSadnicu(sadnica){
    localStorage.setItem('sadnica',JSON.stringify(sadnica));
  }

  getSadnicaInfo(sadnica){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/sadnice/sadnicaInfo',sadnica,{headers:headers});  
  }


  vratiSadnicu(){
    return JSON.parse(localStorage.getItem('sadnica'));
  }

  obirsiSadnicu(){
    localStorage.setItem('sadnica',null);
  }

  getMagacinInfo(usr){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/magacini/magacinInfo',usr,{headers:headers});    
  }


  sortByNaziv(usr){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/magacini/sortirajponazivu',usr,{headers:headers});    
  }

   sortByKolicina(usr){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/magacini/sortirajpokolicini',usr,{headers:headers});    
  }

  sortByProizvodjac(usr){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/magacini/sortirajpoproizvodjacu',usr,{headers:headers});    
  }


  filter(usr){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/magacini/filter',usr,{headers:headers});    
  }


  dodajProizvod(usr){
      let headers = new HttpHeaders();
      headers = headers.append('Content-Type','application/json');
      return this._http.post('http://localhost:3000/magacini/dodajProizvod',usr,{headers:headers});    
  }

  changeQuanity(usr){
    console.log("servis");
    console.log(usr);
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/magacini/changeQuantity',usr,{headers:headers});        
  }

}
