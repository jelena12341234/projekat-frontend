import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private _http: HttpClient,
              private route:Router) { }



  showUsers(){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.get('http://localhost:3000/users/showUsers',{headers:headers});
  }

  

  updateUser(user){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/updateUser', user, {headers: headers});
  }



  authUser(user){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/authenticate', user, {headers: headers});
  }


  register(user){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/register', user, {headers: headers});  
  }


  checkIfUserExists(user){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/check', user, {headers: headers});  
  }


  getRequests(){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.get('http://localhost:3000/users/registerRequests',{headers:headers});
  }

  getPreduzecaRequest(){
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.get('http://localhost:3000/users/registerPreduzeca',{headers:headers});  
  }

  accept(user){

    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/acceptUser', user, {headers: headers});  
  }


  deny(username){
    
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type','application/json');
    return this._http.post('http://localhost:3000/users/denyUser', username, {headers: headers});  

  }


  loggedInUser(){
    return JSON.parse(localStorage.getItem('korisnik'));
  }

  logOut(){
    localStorage.setItem('korisnik',null);
    this.route.navigate(['/']);
  }

}
