 export interface MagacinAnswer{
     nazivRasadnika:string,
     username:string,
     typ:string,
     naziv:string,
     proizvodjac:string,
     kolicina:number,
     arrived:boolean
 }