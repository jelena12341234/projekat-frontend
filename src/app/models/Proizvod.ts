export interface Proizvod{
    proizvodjac : string,
    tip : string,
    naziv : string,
    kolicina : number,
    available : boolean,
    prosecnaOcena: number
}