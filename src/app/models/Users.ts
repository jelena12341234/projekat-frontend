export interface Users{
    ime:string,
    prezime:string,
    naziv:string,
    username:string,
    password:string,
    date:string,
    place:string,
    phone:string,
    mail:string,
    approved:Boolean,
    typeOfUser:string
}