export interface Narudzbina{
    proizvodjac:string,
    tip:string,
    naziv:string,
    kolicina:number,
    username:string,
    datum:string
}