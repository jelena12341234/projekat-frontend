export interface Sadnica{
    nazivRasadnika:string,
    username:string,
    mesto:string,
    naziv:string,
    proizvodjac:string,
    progress:number,
    row:number,
    col:number,
    izvadjena:Boolean
}