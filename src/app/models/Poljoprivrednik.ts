export interface Poljoprivrednik{
          id: string,
          ime: string,
          prezime: string,    
          username: string,
          date: string,
          place:string,
          phone:string,
          mail: string,
          approved: string,
          typeOfUser: string
}