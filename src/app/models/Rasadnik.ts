export interface Rasadnik{
    naziv:string,
    mesto:string,
    brojSadnica:number,
    brojSlMesta:number,
    kolVode:number,
    temperatura:number,
    sirina:number,
    visina:number
}