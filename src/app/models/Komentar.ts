export interface Komentar{
    proizvodjac:string,
    naziv:string,
    username:string,
    komentar:string,
    ocena:number
}