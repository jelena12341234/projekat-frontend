import { Component, OnInit } from '@angular/core';
import { ShopService } from '../services/shop.service';
import { AuthServiceService } from '../services/auth-service.service';
import { user } from '../models/user';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  korisnik: user;

  naziv: string;
  kolicina: number;
  cena: number;
  tip: string;

  constructor(private myShop: ShopService,
    private myAuthService: AuthServiceService) { }

  ngOnInit(): void {
    this.korisnik = this.myAuthService.loggedInUser();
    console.log(this.korisnik);
  }

  addProduct() {
    let type;
    if (this.tip=="S") {type="sadnica";}
      else if(this.tip == "P") {type="preparat"}
       let ras = {
        proizvodjac: this.korisnik.username,
        tip: type,
        naziv:this.naziv,
        kolicina: this.kolicina,
        available : this.kolicina > 0 ? true : false,
        prosecnaOcena : 0
      }


      this.myShop.dodajProizvod(ras).subscribe(err=>{
        console.log(err);
      });

   location.reload();
  }

  logout(){
    this.myAuthService.logOut();
  }

}
