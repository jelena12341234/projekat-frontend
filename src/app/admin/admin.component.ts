import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthServiceService } from '../services/auth-service.service';
import { Users } from '../models/Users';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private myAuthService: AuthServiceService) { }


  currentUser:Users;

  users: Users[];
  preduzeca: Users[];

  korisnici: Users[];

  naziv:string;
  skracenica:string;
  lozinka:string;
  datum:string;
  mesto:string;
  mejl:string;


  ime:string;
  prezime:string;
  lozinkaPolj:string;
  korime:string;
  telefon:string;
  datumPolj:string;
  mestoPolj:string;
  mejlPolj:string;


  updateUser(usr){
    this.currentUser = usr;
  }




  logout(){
    this.myAuthService.logOut();
  }


  registrujPolj(){

    let user ={
      prezime:this.prezime,
      ime:this.ime,
      username:this.korime,
      password:this.lozinkaPolj,
      date:this.datumPolj,
      place:this.mestoPolj,
      phone:this.telefon,
      mail:this.mejlPolj,
      approved:true,
      typeOfUser:"poljoprivrednik"
    }


    this.myAuthService.accept(user).subscribe(err=>{
      console.log(err);
    });
    location.reload();

  }

  registrujPred(){

    let user ={
      naziv:this.naziv,
      username:this.skracenica,
      password:this.lozinka,
      date:this.datum,
      place:this.mesto,
      mail:this.mejl,
      approved:true,
      typeOfUser:"preduzece"
    }


    this.myAuthService.accept(user).subscribe(err=>{
      console.log(err);
    });
    location.reload();
  }


  update(user){
    //alert("Hello" + user.username);

    this.myAuthService.updateUser(user).subscribe(data=>{
      console.log(data);
      location.reload();
    },err=>{
      console.log(err);
    });

  }



  ngOnInit(): void {

    this.myAuthService.showUsers().subscribe((data:Users[])=>{
      this.korisnici=data;
      this.currentUser=data[0];
    }, err=>{
      console.log(err);
    });

  this.myAuthService.getRequests().subscribe((data: Users[])=>{
    this.users = data;
  }, err=>{
    console.log(err);
    return false;
  });
  

  this.myAuthService.getPreduzecaRequest().subscribe((data1: Users[])=>{
    this.preduzeca = data1;
  }, err=>{
    console.log(err);
    return false;
  });
  
  }


  odobri(korIme){
    const user = {
      username : korIme
    }

    this.myAuthService.accept(user).subscribe(err=>{
      console.log(err);
    });
    location.reload();
  }

  odbij(korIme){
    const user = {
      username : korIme
    }

    this.myAuthService.deny(user).subscribe(err=>{
      console.log(err);
    });
    
    location.reload();
  }

}
