import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { LoginAnswer } from '../models/LoginAnswer';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private myAuthService : AuthServiceService,
              private myRoute:Router
              ) { }

  ngOnInit(): void {
  }

  korime:string;
  lozinka:string;
  
  login(){

    
    const user = {
      username: this.korime,
      password: this.lozinka
    }

    
    this.myAuthService.authUser(user).subscribe((data:LoginAnswer)=>{
        if(data.success == true){
          localStorage.setItem('korisnik',JSON.stringify(user));
          if(data.user.typeOfUser == "poljoprivrednik"){
              this.myRoute.navigate(['/poljoprivrednik']);
          }else if(data.user.typeOfUser == "preduzece") {
            this.myRoute.navigate(['/preduzece']);
          }else if(data.user.typeOfUser == "admin") {
            this.myRoute.navigate(['/admin']);
          }
        
        }
        else{
          alert("falseee bleeeh");
        }
    });


  }

}
