import { Component, OnInit } from '@angular/core';
import { Proizvod } from '../models/Proizvod';
import { ShopService } from '../services/shop.service';
import { Komentar } from '../models/Komentar';
import { RasadniciServiceService } from '../services/rasadnici-service.service';
import { Rasadnik } from '../models/Rasadnik';
import { AuthServiceService } from '../services/auth-service.service';
import { user } from '../models/user';

@Component({
  selector: 'app-online-prodavnica',
  templateUrl: './online-prodavnica.component.html',
  styleUrls: ['./online-prodavnica.component.css']
})
export class OnlineProdavnicaComponent implements OnInit {

  constructor(private myShopService: ShopService,
              private myRasService: RasadniciServiceService,
              private myAuthService: AuthServiceService) { }

  proizvodi:Proizvod[]=[];
  flags:Boolean[] = [];
  komentari:Komentar[]=[];

  rasadnik:Rasadnik;
  korisnik:user;

  
  kolicineProizvoda:number[]=[];
  checkArray:Boolean[]=[];

  ngOnInit(): void {
    this.rasadnik = this.myRasService.vratiRasadnik();
    this.korisnik = this.myAuthService.loggedInUser();
    this.myShopService.vratiProizvode().subscribe((data:Proizvod[])=>{
      this.proizvodi=data; 
      this.proizvodi.sort((n1,n2)=>{
        if(n1.proizvodjac > n2.proizvodjac) return 1;
        if(n1.proizvodjac < n2.proizvodjac) return -1;
        return 0;
      });
      
      
      let i = 0;
      this.proizvodi.forEach(elem=>{
        this.flags[i]=false; 
        this.checkArray.push(false);
        this.kolicineProizvoda.push(0); 
        i++;
      });
   },err=>{
    console.log(err);
    return false;
   });

  }


  showInfo(ras,i){

    let index=0;
    this.proizvodi.forEach(elem=>{
      this.flags[index]=false; 
      index++;
    });
    
    console.log(this.flags);
    this.flags[i] = true;

    console.log(this.flags);
    
    this.myShopService.getComments(ras).subscribe((data:Komentar[])=>{
      this.komentari=data; 
   },err=>{
    console.log(err);
    return false;
   });  
  }

  buy(){


    for(let i=0; i<this.checkArray.length; i++){
      if(this.checkArray[i]){
        if(this.kolicineProizvoda[i] > this.proizvodi[i].kolicina){ alert("Ne mozete izabrati vise od "+this.proizvodi[i].kolicina+ " "+ this.proizvodi[i].naziv+"a" );
        return;
        }
        if(this.kolicineProizvoda[i]<0){ alert("Molimo Vas, izaberite pozitivan broj"); return;}
       }
    }

    for(let i=0; i<this.checkArray.length; i++){
      if(this.checkArray[i]){
      
      let mag = {
      nazivRasadnika: this.rasadnik.naziv,
      username: this.korisnik.username,
      tip: this.proizvodi[i].tip,
      proizvodjac: this.proizvodi[i].proizvodjac,
      kolicina: this.kolicineProizvoda[i],
      arrived : false
      }

      console.log(mag);

//      this.myRasService.dodajProizvod(mag).subscribe(err=>{
 //       if(err) console.log(err);
  //    });

      }
    }
  
    
    let proizvodjaci: string[]=[];

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');  
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    let datumToday = yyyy+ "-" +mm + "-"+dd;

      let i=0;

      /*

      while(i < this.checkArray.length){
      let proizvodjac = this.proizvodi[i].proizvodjac;

      let order = {
        proizvodjac: this.proizvodi[i].proizvodjac,
        username:this.korisnik.username,
        datum:datumToday,
        tip:this.proizvodi[i].tip,
        kolicina:this.proizvodi[i].kolicina
      }




      i++;

      while(this.proizvodi[i].proizvodjac == proizvodjac && i < this.checkArray.length){
        order.kolicina = order.kolicina + this.proizvodi[i].kolicina;
      }
      
      console.log(order);
        /*
         proizvodjac : req.body.proizvodjac,
       tip : req.body.tip,
      naziv : req.body.naziv,
      kolicina : req.body.kolicina,
      username: req.body.username,
      datum: req.body.datum
      
        
    }
    */  
  
  
  }

}
