import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineProdavnicaComponent } from './online-prodavnica.component';

describe('OnlineProdavnicaComponent', () => {
  let component: OnlineProdavnicaComponent;
  let fixture: ComponentFixture<OnlineProdavnicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineProdavnicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineProdavnicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
