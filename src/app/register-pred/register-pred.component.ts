import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { CheckAnswer } from '../models/CheckAnswer';

@Component({
  selector: 'app-register-pred',
  templateUrl: './register-pred.component.html',
  styleUrls: ['./register-pred.component.css']
})
export class RegisterPredComponent implements OnInit {

  constructor(private myAuthService : AuthServiceService) { }

  ngOnInit(): void {
  }

  naziv:string;
  skracenica:string;
  lozinka:string;
  ponlozinka:string;
  datum:string;
  mesto:string;
  mejl:string;

  message:string;
  captchaS:string;

  recaptcha: any[];
  resolved(captchaResponse: any[]){
    this.captchaS = "OK";
    this.recaptcha= captchaResponse;
    console.log(this.recaptcha);


  }

  registruj(){
    if(this.captchaS != "OK") {  
    //  this.message="robot";
      return;
    }
    if(this.datum==undefined || this.naziv==undefined || this.lozinka==undefined || this.mejl==undefined ||
      this.mesto==undefined || this.ponlozinka==undefined) {
      this.message = "Treba popuniti sva polja";
      return;
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if( re.test(String(this.mejl).toLowerCase()) == false){
      this.message = "Uneta mejl adresa nije validna!";
      return;
    }

    if(this.lozinka != this.ponlozinka) {
      this.message= "Unete lozinke nisu iste!";
      return;
    }

    const user = {
      naziv : this.naziv,
      username: this.skracenica,
      password: this.lozinka,
      date: this.datum,
      place: this.mesto,
      mail: this.mejl, 
      approved: false,
      typeOfUser: "preduzece"
    }

    
    this.myAuthService.checkIfUserExists(user).subscribe((answer:CheckAnswer)=>{
      if(answer.success == true){
        alert("postoji korisnik sa tim imenom");
        return;
      } 
    });

    
    this.myAuthService.register(user).subscribe((answer:CheckAnswer)=>{
      if(answer.success == true){
        location.reload();
      }
      else { 
        this.message="Registrovanje nije uspesno";
    }
    });
    

  }

}
