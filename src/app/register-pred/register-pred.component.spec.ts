import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterPredComponent } from './register-pred.component';

describe('RegisterPredComponent', () => {
  let component: RegisterPredComponent;
  let fixture: ComponentFixture<RegisterPredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterPredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterPredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
