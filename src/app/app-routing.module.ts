import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RegisterPredComponent } from './register-pred/register-pred.component';
import { AdminComponent } from './admin/admin.component';
import { PoljoprivrednikComponent } from './poljoprivrednik/poljoprivrednik.component';
import { PreduzeceComponent } from './preduzece/preduzece.component';
import { RasadnikComponent } from './rasadnik/rasadnik.component';
import { DodajRasadnikComponent } from './dodaj-rasadnik/dodaj-rasadnik.component';
import { MagacinComponent } from './magacin/magacin.component';
import { OnlineProdavnicaComponent } from './online-prodavnica/online-prodavnica.component';
import { ProizvodiInfoComponent } from './proizvodi-info/proizvodi-info.component';
import { PogledajproizvodeComponent } from './pogledajproizvode/pogledajproizvode.component';
import { AddProductComponent } from './add-product/add-product.component';
import { HomeComponent } from './home/home.component';
import { DataVisualComponent } from './data-visual/data-visual.component';


const routes: Routes = [
  {path:'prijava', component:LoginComponent},
  {path:'registracija', component:RegisterComponent},
  {path:'registracijapreduzeca', component:RegisterPredComponent},
  {path:'admin', component:AdminComponent},
  {path:'poljoprivrednik', component:PoljoprivrednikComponent},
  {path:'preduzece', component:PreduzeceComponent},
  {path:'rasadnik', component:RasadnikComponent},
  {path:'dodajras', component:DodajRasadnikComponent},
  {path:'magacin', component:MagacinComponent},
  {path:'onlineprod',component:OnlineProdavnicaComponent},
  {path:'mojiproizvodi', component:ProizvodiInfoComponent},
  {path:'viewproduct',component:PogledajproizvodeComponent},
  {path:'addproduct', component:AddProductComponent},
  {path:'', component:HomeComponent},
  {path:'data',component:DataVisualComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
